// Fill out your copyright notice in the Description page of Project Settings.


#include "Input/AuraInputConfig.h"


const UInputAction* UAuraInputConfig::FindAbilityInputActionForTag(const FGameplayTag& InputTag, bool bLogNotFound) const
{
	for (const FAuraInputAction& InputActionStruct : AbilityInputActions)
	{
		if(InputActionStruct.InputAction && InputActionStruct.InputTag == InputTag)
		{
			return InputActionStruct.InputAction;
		}
	}

	if (bLogNotFound)
	{
		UE_LOG(LogTemp, Error, TEXT("Not found input action for tag:%s in InputConfig: %s"), *InputTag.ToString(), *GetNameSafe(this));
	}

	return nullptr;
}
