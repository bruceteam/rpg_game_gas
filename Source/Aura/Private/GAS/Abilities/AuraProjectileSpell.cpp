// Fill out your copyright notice in the Description page of Project Settings.


#include "GAS/Abilities/AuraProjectileSpell.h"
#include "Actor/AuraProjectile.h"
#include "Interaction/CombatInterface.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "AuraGameplayTags.h"


void UAuraProjectileSpell::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	//print string
	//UKismetSystemLibrary::PrintString(this, FString("ActivateAbility C++"), true, true, FLinearColor::Yellow,3.f);
}

void UAuraProjectileSpell::SpawnProjectile(const FVector& ProjectileTargetLocation, const FGameplayTag& SocketTag, bool bOverridePitch, float PitchOverride)
{
	const bool bIsServer = GetAvatarActorFromActorInfo()->HasAuthority();
	if (!bIsServer) return;

	ICombatInterface* CombatInterface = Cast<ICombatInterface>(GetAvatarActorFromActorInfo());
	if (CombatInterface)
	{
		const FVector SocketLocation = ICombatInterface::Execute_GetCombatSocketLocation(GetAvatarActorFromActorInfo(),//
		SocketTag);
		FRotator Rotation = (ProjectileTargetLocation - SocketLocation).Rotation();
		if (bOverridePitch)
		{
			Rotation.Pitch = PitchOverride;
		}

		FTransform SpawnTransform;
		SpawnTransform.SetLocation(SocketLocation);
		SpawnTransform.SetRotation(Rotation.Quaternion());

		AAuraProjectile* Projectile = GetWorld()->SpawnActorDeferred<AAuraProjectile>(
			ProjectileClass,
			SpawnTransform,
			GetOwningActorFromActorInfo(),
			Cast<APawn>(GetOwningActorFromActorInfo()),
			ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

		Projectile->DamageEffectParams = MakeDamageEffectParamsFromClassDefaults();

		Projectile->FinishSpawning(SpawnTransform);
	}
}
			/* OLD AFTER SPAWNACTORDEFFERED AND BEFORE FINISHSPAWNING*/
		////give the projectile a gameplay effect spec for causing damage
		//const UAbilitySystemComponent* SourseASC = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(GetAvatarActorFromActorInfo());
		//if (SourseASC)
		//{
		//	FGameplayEffectContextHandle EffectContextHandle = SourseASC->MakeEffectContext();

		//	//EffectContextHandle.SetAbility(this);
		//	//EffectContextHandle.AddSourceObject(Projectile);
		//	//TArray<TWeakObjectPtr<AActor>> Actors;
		//	//Actors.Add(Projectile);
		//	//EffectContextHandle.AddActors(Actors);


		//	const FGameplayEffectSpecHandle SpecHandle = SourseASC->MakeOutgoingSpec(DamageEffectClass, GetAbilityLevel(), EffectContextHandle);
		//	const FAuraGameplayTags GameplayTags = FAuraGameplayTags::Get();

		//	const float ScaledDamage = Damage.GetValueAtLevel(GetAbilityLevel());
		//	UAbilitySystemBlueprintLibrary::AssignTagSetByCallerMagnitude(SpecHandle, DamageType, ScaledDamage);
		//	//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Blue, FString::Printf(TEXT("FireBall damage = %f"), ScaledDamage));
		//	
		//	Projectile->DamageEffectSpecHandle = SpecHandle;
		//}