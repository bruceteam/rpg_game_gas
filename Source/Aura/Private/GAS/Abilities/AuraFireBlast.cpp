// Fill out your copyright notice in the Description page of Project Settings.


#include "GAS/Abilities/AuraFireBlast.h"
#include "GAS/AuraAbilitySystemLibrary.h"
#include "Actor/AuraBoltOfFIre.h"

TArray<AAuraBoltOfFIre*> UAuraFireBlast::SpawnBoltsOfFire()
{
	TArray<AAuraBoltOfFIre*> BoltsOfFire;
	const FVector Forward = GetAvatarActorFromActorInfo()->GetActorForwardVector();
	const FVector Location = GetAvatarActorFromActorInfo()->GetActorLocation();
	const TArray<FRotator> Rotators = UAuraAbilitySystemLibrary::EvenlySpaceRotators(Forward, FVector::UpVector, 360.f, NumFireblastBalls);

	for (const FRotator& Rotator : Rotators)
	{
		if (GetWorld())
		{
			FTransform SpawnTransform;
			SpawnTransform.SetLocation(Location);
			SpawnTransform.SetRotation(Rotator.Quaternion());

			AAuraBoltOfFIre* Bolt = GetWorld()->SpawnActorDeferred<AAuraBoltOfFIre>(
				BoltOfFireClass,
				SpawnTransform,
				GetOwningActorFromActorInfo(),
				CurrentActorInfo->PlayerController->GetPawn(),
				ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

			Bolt->DamageEffectParams = MakeDamageEffectParamsFromClassDefaults();
			Bolt->ReturnToActor = GetAvatarActorFromActorInfo();
			Bolt->SetOwner(GetAvatarActorFromActorInfo());

			Bolt->ExplosionDamageParams = MakeDamageEffectParamsFromClassDefaults();
			Bolt->SetOwner(GetAvatarActorFromActorInfo());

			BoltsOfFire.Add(Bolt);

			Bolt->FinishSpawning(SpawnTransform);
		}
	}

	return BoltsOfFire;
}
