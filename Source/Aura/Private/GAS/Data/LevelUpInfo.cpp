// Fill out your copyright notice in the Description page of Project Settings.


#include "GAS/Data/LevelUpInfo.h"

int32 ULevelUpInfo::FindLevelForXP(int32 XP) const
{
	bool bSearching = true;
	int32 Level = 1;

	while (bSearching)
	{
		if (LevelUpInfos.Num() - 1 <= Level) return Level;

		if (XP >= LevelUpInfos[Level].LevelUpRequirement)
		{
			++Level;
		}
		else
		{
			bSearching = false;
		}
	}

	return Level;
}
