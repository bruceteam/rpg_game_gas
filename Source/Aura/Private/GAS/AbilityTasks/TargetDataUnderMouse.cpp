// Fill out your copyright notice in the Description page of Project Settings.


#include "GAS/AbilityTasks/TargetDataUnderMouse.h"
#include "AbilitySystemComponent.h"
#include "Aura/Aura.h"

UTargetDataUnderMouse* UTargetDataUnderMouse::CreateTargetDataUnderMouse(UGameplayAbility* OwningAbility)
{
	UTargetDataUnderMouse* MyObj = NewAbilityTask<UTargetDataUnderMouse>(OwningAbility);
	return MyObj;

}

void UTargetDataUnderMouse::Activate()
{
	const bool bUsLocallyControlled = Ability->GetCurrentActorInfo()->IsLocallyControlled();

	if (bUsLocallyControlled)
	{
		SendMouseCursorData();
	}
	else
	{
		//we are on the server, so listen for target data
		const FGameplayAbilitySpecHandle SpecHandle = GetAbilitySpecHandle();
		const FPredictionKey ActivationPredictionKey = GetActivationPredictionKey();

		AbilitySystemComponent.Get()->AbilityTargetDataSetDelegate(SpecHandle, ActivationPredictionKey)//
			.AddUObject(this, &UTargetDataUnderMouse::OnTargetDataReplicatedCallback);

		//what if we allready received TargetData
		const bool bCalledDelegate = AbilitySystemComponent.Get()->CallReplicatedTargetDataDelegatesIfSet(SpecHandle, ActivationPredictionKey);
	
		if (!bCalledDelegate)
		{
			SetWaitingOnRemotePlayerData();
		}
	}
}

void UTargetDataUnderMouse::SendMouseCursorData()
{
	//that means that everything  is below are predicted, like "Server,pls let us do what we want"
	FScopedPredictionWindow ScopedPrediction(AbilitySystemComponent.Get());

	//creating new pbject defined in class GameplayAbilityTargetDataTypes

	APlayerController* PC = Ability->GetCurrentActorInfo()->PlayerController.Get();
	FHitResult Hit;
	PC->GetHitResultUnderCursor(ECC_Target, false, Hit);

	FGameplayAbilityTargetDataHandle DataHandle;
	FGameplayAbilityTargetData_SingleTargetHit* Data = new FGameplayAbilityTargetData_SingleTargetHit();
	Data->HitResult = Hit;
	DataHandle.Add(Data);

	AbilitySystemComponent->ServerSetReplicatedTargetData(
							GetAbilitySpecHandle(), 
							GetActivationPredictionKey(),
							DataHandle, 
							FGameplayTag(),
							AbilitySystemComponent->ScopedPredictionKey);


	if (ShouldBroadcastAbilityTaskDelegates())
	{
		ValidData.Broadcast(DataHandle);
	}
}

void UTargetDataUnderMouse::OnTargetDataReplicatedCallback(const FGameplayAbilityTargetDataHandle& DataHandle, FGameplayTag ActivationTag)
{
	//this talled to abilitysystem that target data is received pls don't need to store it
	AbilitySystemComponent->ConsumeClientReplicatedTargetData(GetAbilitySpecHandle(), GetActivationPredictionKey());

	if (ShouldBroadcastAbilityTaskDelegates())
	{
		ValidData.Broadcast(DataHandle);
	}
}
