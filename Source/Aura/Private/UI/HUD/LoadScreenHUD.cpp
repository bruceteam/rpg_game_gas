// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/HUD/LoadScreenHUD.h"
#include "Blueprint/UserWidget.h"
#include "UI/Widgets/LoadScreenWidget.h"
#include "UI/ViewModel/MVVM_LoadScreen.h"

void ALoadScreenHUD::BeginPlay()
{
	Super::BeginPlay();

	if (!GetWorld()) return;

	LoadScreenViewModel = NewObject<UMVVM_LoadScreen>(this, LoadScreenViewModelClass);
	if (LoadScreenViewModel)
	{
		LoadScreenViewModel->InitializeLoadSlots();
	}

	LoadScreenWidget = CreateWidget<ULoadScreenWidget>(GetWorld(), LoadScreenWidgetClass);
	if (LoadScreenWidget)
	{
		LoadScreenWidget->AddToViewport();
		LoadScreenWidget->BlueprintInitializeWidget();
	}

	if (LoadScreenViewModel)
	{
		LoadScreenViewModel->LoadData();
	}
}
