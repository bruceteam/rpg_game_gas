// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GAS/Abilities/AuraDamageGameplayAbility.h"
#include "AuraFireBlast.generated.h"

class AAuraBoltOfFIre;

UCLASS()
class AURA_API UAuraFireBlast : public UAuraDamageGameplayAbility
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	TArray<AAuraBoltOfFIre*> SpawnBoltsOfFire();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Fireblast")
	int32 NumFireblastBalls = 12;
	
private:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AAuraBoltOfFIre> BoltOfFireClass;
};
