// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GAS/Abilities/AuraProjectileSpell.h"
#include "AuraFireball.generated.h"

/**
 * 
 */
UCLASS()
class AURA_API UAuraFireball : public UAuraProjectileSpell
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	void SpawnProjectiles(const FVector& ProjectileTargetLocation, const FGameplayTag& SocketTag, bool bOverridePitch, float PitchOverride, AActor* HomingTarget);

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Fireball")
	float ProjectileSpread = 90.f;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Fireball")
	int32 MaxNumProjectiles = 5;

	UPROPERTY(EditDefaultsOnly, Category = "Fireball")
	float HomingAccelerationMin = 1600.f;

	UPROPERTY(EditDefaultsOnly, Category = "Fireball")
	float HomingAccelerationMax = 3200.f;

	UPROPERTY(EditDefaultsOnly, Category = "Fireball")
	bool bLaunchHomingProjectiles = true;
};
