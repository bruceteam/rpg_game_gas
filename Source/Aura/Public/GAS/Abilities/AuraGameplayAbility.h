// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "AuraGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class AURA_API UAuraGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditDefaultsOnly, Category = "Input")
	FGameplayTag StartupInputTag;

	UFUNCTION(BlueprintNativeEvent)
	FString GetDescription(int32 Level);
	UFUNCTION(BlueprintNativeEvent)
	FString GetNextLevelDescription(int32 Level);

	static FString GetLockedDescription(int32 Level);

protected:
	UFUNCTION(BlueprintPure)
	float GetManaCost(float InLevel = 1.0f) const;

	UFUNCTION(BlueprintPure)
	float GetCooldown(float InLevel = 1.0f) const;
};
