// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actor/AuraCheckpoint.h"
#include "AuraMapEntrance.generated.h"

UCLASS()
class AURA_API AAuraMapEntrance : public AAuraCheckpoint
{
	GENERATED_BODY()
	
public:
	AAuraMapEntrance(const FObjectInitializer& ObjectInitializer);

	/*Highlight interface*/
	virtual void HighlightActor_Implementation() override;
	/*End Highlight interface*/

	/*Saved interface*/
	virtual void LoadActor_Implementation();
	/*End saved interface*/

	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UWorld> DestinationMap;

	UPROPERTY(EditAnywhere)
	FName DestinationPlayerStartTag;

protected:
	virtual void OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

};
